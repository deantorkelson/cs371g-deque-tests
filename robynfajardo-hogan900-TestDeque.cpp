// -------------
// TestDeque.c++
// -------------

// http://www.cplusplus.com/reference/deque/deque/

// --------
// includes
// --------

#include <deque>     // deque
#include <stdexcept> // invalid_argument, out_of_range

#include "gtest/gtest.h"

#include "Deque.hpp"

// -----
// Using
// -----

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct DequeFixture : Test {
    using deque_type    = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator       = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;
};

using
deque_types =
    Types<
    // deque<int>,
    my_deque<int>,
    // deque<int, allocator<int>>>;
    my_deque<int, allocator<int>>>;


#ifdef __APPLE__
TYPED_TEST_CASE(DequeFixture, deque_types);
#else
TYPED_TEST_CASE(DequeFixture, deque_types);
#endif

// -----
// Tests
// -----

TYPED_TEST(DequeFixture, test1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0u);
}

TYPED_TEST(DequeFixture, test2) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    iterator b = x.begin();
    iterator e = x.end();
    bool a = b == e;
    ASSERT_EQ(a, true);
}

TYPED_TEST(DequeFixture, test3) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x;
    const_iterator b = x.begin();
    const_iterator e = x.end();
    ASSERT_EQ(b, e);
}

TYPED_TEST(DequeFixture, test4) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    deque_type y;
    ASSERT_EQ(x.size(), y.size());
}


TYPED_TEST(DequeFixture, test5) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    deque_type y;
    y.push_back(2);
    bool not_equal = x != y;
    ASSERT_EQ(not_equal, true);
}

TYPED_TEST(DequeFixture, test6) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    deque_type y;
    bool less = x < y;
    ASSERT_EQ(less, false);
}

TYPED_TEST(DequeFixture, test7) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_front(2);
    x.push_back(6);
    int f = x.front();
    int e = x.back();
    ASSERT_EQ(f, 2);
    ASSERT_EQ(e, 6);
}

TYPED_TEST(DequeFixture, test8) {
    using deque_type = typename TestFixture::deque_type;
    using const_iterator   = typename TestFixture::const_iterator;
    deque_type x;
    x.push_front(2);
    x.push_front(5);
    x.push_back(6);
    x.push_back(7);
    const deque_type y = x;
    const_iterator f = y.begin();
    const_iterator e = y.end();
    --e;
    ASSERT_EQ(*f, 5);
    ASSERT_EQ(*e, 7);}

TYPED_TEST(DequeFixture, test9) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_front(2);
    x.push_back(8);
    x.push_back(6);
    int f = x[1];
    ASSERT_EQ(f, 8);
}

TYPED_TEST(DequeFixture, test10) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_front(12);
    x.push_back(23);
    x.push_back(54);
    x.push_back(45);
    int a = x.at(1);
    int f = x.front();
    int e = x.back();
    ASSERT_EQ(a, 23);
    ASSERT_EQ(f, 12);
    ASSERT_EQ(e, 45);
}

TYPED_TEST(DequeFixture, test11) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x (10, 2);
    x.clear();
    ASSERT_EQ(x.size(), 0);}

TYPED_TEST(DequeFixture, test12) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(6562);
    x.push_back(27382);
    x.push_back(347824);
    x.pop_back();
    int e = x.back();
    ASSERT_EQ(e, 27382);
}

TYPED_TEST(DequeFixture, test13) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x (20, 2);
    iterator b = x.begin();
    iterator e = x.end();
    ++b;
    ++b;
    ++b;
    ++b;
    *b += 28;
    ASSERT_EQ(*b, 30);}

TYPED_TEST(DequeFixture, test14) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x (20, 2);
    iterator b = x.begin();
    iterator e = x.end();
    --e;
    --e;
    --e;
    --e;
    *e += 28;
    ASSERT_EQ(*e, 30);}

TYPED_TEST(DequeFixture, test15) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    x.push_back(23);
    x.push_back(193);
    x.push_back(54);
    x.push_front(13);
    iterator b = x.begin();
    ++++b;
    ASSERT_EQ(*b, 193);}

TYPED_TEST(DequeFixture, test16) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(12);
    ASSERT_EQ(x.front(), 12);
}

TYPED_TEST(DequeFixture, test17) {
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x (10, 25678);
    x.push_front(2);
    x.push_back(234);
    iterator b = x.begin();
    *b += 5640;
    ASSERT_EQ(*b, 5642);
    b++;
    ASSERT_EQ(*b, 25678);}

TYPED_TEST(DequeFixture, test18) {
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x (10, 2354);
    x.push_back(2);
    x.push_back(234);
    iterator b = x.begin();
    ++++b;
    *b -= 5;
    ASSERT_EQ(*b, 2349);}

TYPED_TEST(DequeFixture, test19) {
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x (10, 2354);
    x.push_back(2);
    x.push_back(234);
    iterator b = x.begin();
    ++++b;
    *b += 5;
    ASSERT_EQ(*b, 2359);}

TYPED_TEST(DequeFixture, test20) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x (10, 254786);
    x.push_front(2);
    x.push_back(6);
    int f = x.front();
    int e = x.back();
    int mid = x.at(x.size() / 2);
    ASSERT_EQ(f, 2);
    ASSERT_EQ(e, 6);
    ASSERT_EQ(mid, 254786);}

TYPED_TEST(DequeFixture, test21) {
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x (10, 2354);
    x.push_back(2);
    x.push_back(234);
    iterator b = x.end();
    ----b;
    ASSERT_EQ(*b, 2);}

TYPED_TEST(DequeFixture, test22) {
    using deque_type = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    deque_type x (10, 2354);
    x.push_front(2);
    x.push_front(234);
    const deque_type y = x;
    const_iterator b = y.begin();
    ++++b;
    ASSERT_EQ(*b, 2354);}

TYPED_TEST(DequeFixture, test23) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x (10, 2357);
    x.push_back(234);
    int val = x.at(x.size() - 1);
    ASSERT_EQ(val, 234);}

TYPED_TEST(DequeFixture, test24) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_front(2);
    x.push_back(6);
    x.push_back(214);
    x.push_back(12);
    int val = x.at(2);
    ASSERT_EQ(val, 214);
}

TYPED_TEST(DequeFixture, test25) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    ASSERT_EQ(x.empty(), true);
}

TYPED_TEST(DequeFixture, test26) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x (10, 2);
    iterator b = begin(x);
    ++++b;
    x.erase(b);
    ASSERT_EQ(x.size(), 9);}

TYPED_TEST(DequeFixture, test27) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_front(2);
    x.push_back(6);
    x.push_front(345);
    x.push_front(685);
    x.pop_front();
    int f = x.front();
    ASSERT_EQ(f, 345);}

TYPED_TEST(DequeFixture, test28) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x (10);
    x.push_front(2);
    x.push_back(6);
    x.push_front(3456);
    x.push_back(3657);
    x.pop_back();
    int f = x.back();
    ASSERT_EQ(x.back(), 6);}

TYPED_TEST(DequeFixture, test29) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_front(2);
    x.push_back(6);
    x.push_back(32);
    x.resize(1);
    int size = x.size();
    ASSERT_EQ(size, 1);
}

TYPED_TEST(DequeFixture, test30) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_front(2);
    x.push_back(6);
    int f = x.front();
    int e = x.back();
    int size = x.size();
    ASSERT_EQ(size, 2);
}

TYPED_TEST(DequeFixture, test31) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x (10, 2);
    ASSERT_EQ(x.size(), 10);
}


TYPED_TEST(DequeFixture, test32) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x (10, 2);
    x.push_back(253);
    x.push_back(2363);
    x.push_front(14365);
    iterator it = x.begin();
    it += 5;
    x.insert(it, 345);
    ASSERT_EQ(x.at(5), 345);
}

TYPED_TEST(DequeFixture, test33) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x (10, 2);
    x.push_back(253);
    x.push_back(2363);
    x.push_front(14365);
    deque_type y = x;
    ASSERT_EQ(x, y);
}

TYPED_TEST(DequeFixture, test34) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x (10, 2);
    x.push_back(253);
    x.push_back(2363);
    x.push_front(14365);
    iterator it = x.end();
    it -= 5;
    ASSERT_EQ(*it, 2);
}

TYPED_TEST(DequeFixture, test35) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x (10, 2);
    x.push_back(253);
    x.push_back(2363);
    x.push_front(14365);
    //last element
    iterator it = --x.end();
    ASSERT_EQ(*it, 2363);
}
