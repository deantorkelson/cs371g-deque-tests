// -------------
// TestDeque.c++
// -------------

// http://www.cplusplus.com/reference/deque/deque/

// --------
// includes
// --------

#include <deque>     // deque
#include <stdexcept> // invalid_argument, out_of_range

#include "gtest/gtest.h"

#include "Deque.hpp"

// -----
// Using
// -----

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct DequeFixture : Test {
    using deque_type    = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;};

using
    deque_types =
    Types<
           //deque<int>
        my_deque<int>//,
           //deque<int, allocator<int>>,
        //my_deque<int, allocator<int>>
        >;

#ifdef __APPLE__
    TYPED_TEST_CASE(DequeFixture, deque_types,);
#else
    TYPED_TEST_CASE(DequeFixture, deque_types);
#endif

// -----
// Tests
// -----

TYPED_TEST(DequeFixture, test1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    //ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0u);}

TYPED_TEST(DequeFixture, test2) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    iterator b = begin(x);
    iterator e = end(x);
    ASSERT_EQ(b, e);}

TYPED_TEST(DequeFixture, test3) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x;//(0,0)
    const_iterator b = begin(x);
    const_iterator e = end(x);
    ASSERT_EQ(b, e);}

TYPED_TEST(DequeFixture, test4) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x;
    deque_type y;
    x.push_back(0);
    y.push_back(10);
    ASSERT_LT(x, y);}

TYPED_TEST(DequeFixture, test5) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x;
    deque_type y;
    x.push_back(100);
    y.push_back(0);
    y.push_back(1);
    bool b = (x < y);
    ASSERT_LT(b, true);
}

TYPED_TEST(DequeFixture, test6) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x;
    deque_type y;
    x.push_back(100);
    x.push_back(101);
    y.push_back(100);
    y.push_back(101);
    ASSERT_EQ(x, y);
    ASSERT_EQ(x.back(), 101);
    ASSERT_EQ(x[0], 100);
    ASSERT_EQ(x[1], 101); 
}

TYPED_TEST(DequeFixture, test7) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x;
    deque_type y;
    x.push_back(100);
    y.push_back(100);
    bool b = (x == y);
    ASSERT_EQ(b, true);}

TYPED_TEST(DequeFixture, test8) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x;
    deque_type y;
    x.push_back(1);
    y.push_back(1);
    iterator xi = begin(x);
    iterator yi = begin(y);
    ASSERT_EQ(*xi == *yi, true);}

TYPED_TEST(DequeFixture, test9) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x;
    deque_type y;
    x.push_back(1);
    y.push_back(2);
    iterator xi = begin(x);
    iterator yi = begin(y);
    ASSERT_EQ(*xi == *yi, false);}

TYPED_TEST(DequeFixture, test10) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x;
    x.push_back(1);
    x.push_back(2);
    iterator xi = begin(x);
    ++xi;
    ASSERT_EQ(*xi, 2);}

TYPED_TEST(DequeFixture, test11) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x;
    x.push_back(1);
    x.push_back(2);
    iterator xi = begin(x);
    ++xi;
    --xi;
    ASSERT_EQ(*xi, 1);}
    

TYPED_TEST(DequeFixture, test12) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x;
    x.push_back(1);
    x.push_back(2);
    x.push_back(3);
    iterator xi = begin(x);
    xi = xi + 2;
    ASSERT_EQ(*xi, 3);}

TYPED_TEST(DequeFixture, test13) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x;
    x.push_back(1);
    x.push_back(2);
    x.push_back(3);
    x.push_back(4);
    iterator xi = begin(x);
    xi = xi + 3;
    xi = xi - 2;
    ASSERT_EQ(*xi, 2);}

TYPED_TEST(DequeFixture, test14) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x;
    deque_type* x_ptr = &x;
    ASSERT_EQ(x_ptr->size(), 0);}

TYPED_TEST(DequeFixture, test15) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x;
    x.push_back(1);
    x.push_back(2);
    iterator xi = begin(x);
    xi++;
    xi--;
    ASSERT_EQ(*xi, 1);}

TYPED_TEST(DequeFixture, test16) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x;
    x.push_back(1);
    x.push_back(2);
    x.push_back(3);
    iterator xi = begin(x);
    xi += 2;
    ASSERT_EQ(*xi, 3);}

TYPED_TEST(DequeFixture, test17) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x;
    x.push_back(1);
    ASSERT_EQ(x[0], 1);}

TYPED_TEST(DequeFixture, test18) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x;
    x.push_back(1);
    x.push_back(2);
    iterator xi = begin(x);
    x.insert(xi, 100);
    ASSERT_EQ(x[0], 100);
    ASSERT_EQ(x[1], 1);}

TYPED_TEST(DequeFixture, test19) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x;
    x.push_back(1);
    x.push_front(2);
    ASSERT_EQ(x[0], 2);}

TYPED_TEST(DequeFixture, test20) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x = {1, 2, 3};
    ASSERT_EQ(x[0], 1);
    ASSERT_EQ(x[2], 3);}

TYPED_TEST(DequeFixture, test21) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x = {1, 2, 3};
    const_iterator xi = begin(x);
    ++xi;
    ASSERT_EQ(*xi, 2);}

TYPED_TEST(DequeFixture, test22) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x = {1, 2, 3};
    const_iterator xi = begin(x);
    xi += 2;
    --xi;
    ASSERT_EQ(*xi, 2);}

TYPED_TEST(DequeFixture, test23) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x = {1, 2, 3};
    const_iterator xi = begin(x);
    xi++;
    xi++;
    xi -= 2;
    ASSERT_EQ(*xi, 1);}

TYPED_TEST(DequeFixture, test24) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x = {1, 2, 3};
    const_iterator xi = begin(x);
    xi += 2;
    --xi;
    ASSERT_EQ(*xi, 2);}

TYPED_TEST(DequeFixture, test25) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x = {1, 2, 3};
    const_iterator xi = begin(x);
    xi = xi + 2;
    xi = xi - 2;
    ASSERT_EQ(*xi, 1);}

TYPED_TEST(DequeFixture, test26) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x = {1, 2, 3};
    const deque_type y = {1, 0, 0};
    const_iterator xi = begin(x);
    const_iterator yi = begin(y);;
    ASSERT_EQ((*xi == *yi), true);}

TYPED_TEST(DequeFixture, test27) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    deque_type x(2, 100);
    ASSERT_EQ(x[0], 100);
    ASSERT_EQ(x[1], 100);}

TYPED_TEST(DequeFixture, test28) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x(2, 100);
    ASSERT_EQ(x[0], 100);
    ASSERT_EQ(x[1], 100);}

TYPED_TEST(DequeFixture, test29) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x(100);
    ASSERT_EQ(x.size(), 100);}

TYPED_TEST(DequeFixture, test30) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x;
    x.push_back(0);
    x.push_back(1);
    x.push_front(2);
    x.pop_back();
    ASSERT_EQ(x[1], 0);}

TYPED_TEST(DequeFixture, test31) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x;
    x.push_back(0);
    x.push_back(1);
    x.push_front(2);
    x.pop_front();
    ASSERT_EQ(x[0], 0);}

TYPED_TEST(DequeFixture, test32) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    deque_type x(2, 100);
    ASSERT_EQ(x.front(), 100);}

TYPED_TEST(DequeFixture, test33) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(2, 100);
    x.resize(5, 0);
    ASSERT_EQ(x[0], 100);
    ASSERT_EQ(x[4], 0);}

TYPED_TEST(DequeFixture, test34) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(2, 100);
    x.resize(50, 0);
    ASSERT_EQ(x.size(), 50);
    ASSERT_EQ(x[1], 100);
    ASSERT_EQ(x[44], 0);
}

TYPED_TEST(DequeFixture, test35) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(20, 100);
    x.resize(5, 0);
    ASSERT_EQ(x.size(), 5);
    ASSERT_EQ(x[3], 100);}

TYPED_TEST(DequeFixture, test36) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x;
    x.push_back(0);
    x.push_back(1);
    x.pop_back();
    ASSERT_EQ(x.size(), 1);
    ASSERT_EQ(x.back(), 0);
    ASSERT_EQ(x[0], 0);}

TYPED_TEST(DequeFixture, test37) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x;
    x.push_back(0);
    x.push_back(1);
    x.pop_front();
    ASSERT_EQ(x.size(), 1);
    ASSERT_EQ(x.back(), 1);
    ASSERT_EQ(x.front(), 1);
    ASSERT_EQ(x[0], 1);}

TYPED_TEST(DequeFixture, test38) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x(5, 100);
    x.push_back(50);
    x.pop_front();
    ASSERT_EQ(x.size(), 5);
    ASSERT_EQ(x.back(), 50);
    ASSERT_EQ(x.front(), 100);
    ASSERT_EQ(x[4], 50);}

TYPED_TEST(DequeFixture, test39) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x(2, 100);
    x.pop_front();
    x.push_front(1);
    ASSERT_EQ(x.front(), 1);}

TYPED_TEST(DequeFixture, test40) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x;
    x.push_front(1);
    ASSERT_EQ(x.front(), 1);}

TYPED_TEST(DequeFixture, test41) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x(2,100);
    x.push_front(1);
    x.push_back(2);
    x.pop_front();
    x.push_front(101);
    x.push_front(102);
    ASSERT_EQ(x.front(), 102);
}

TYPED_TEST(DequeFixture, test42) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x = {1, 2, 3, 4, 5};
    deque_type y(12, 150);
    x.swap(y);
    ASSERT_EQ(x.size(), 12);
    ASSERT_EQ(y.size(), 5);
    ASSERT_EQ(y[2], 3);
    ASSERT_EQ(x.front(), 150);
}

TYPED_TEST(DequeFixture, test43) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x = {1, 2, 3, 4, 5};
    deque_type y(x);
    ASSERT_EQ(x, y);
    ASSERT_EQ(y[2], 3);
}

TYPED_TEST(DequeFixture, test44) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x = {1, 2, 3, 4, 5};
    x.clear();
    ASSERT_EQ(x.size(), 0);
}

TYPED_TEST(DequeFixture, test45) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    x.push_back(100);
    x.push_back(100);
    x.push_back(101);
    ASSERT_EQ(x.size(), 12);
    ASSERT_EQ(x.back(), 101);
}

TYPED_TEST(DequeFixture, erase0) { // from public test repo
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x = {3, 1, 4, 4, 2, 9, 3, 7, 1};
    iterator pos = begin(x);

    ++++++pos;
    x.erase(pos);

    deque_type expected = {3, 1, 4, 2, 9, 3, 7, 1};

    ASSERT_TRUE(x.size() == 8);
    ASSERT_EQ(x, expected);
}

TYPED_TEST(DequeFixture, erase1) { // from public test repo
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x = {3, 1, 4, 4, 2, 9, 3, 7, 1};
    iterator pos = end(x);

    ------pos;
    x.erase(pos);

    deque_type expected = {3, 1, 4, 4, 2, 9, 7, 1};

    ASSERT_TRUE(x.size() == 8);
    ASSERT_EQ(x, expected);
}
