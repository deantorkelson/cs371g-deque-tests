// -------------
// TestDeque.c++
// -------------

// http://www.cplusplus.com/reference/deque/deque/

// --------
// includes
// --------

#include <deque>     // deque
#include <stdexcept> // invalid_argument, out_of_range

#include "gtest/gtest.h"

#include "Deque.hpp"

// -----
// Using
// -----

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct DequeFixture : Test {
    using deque_type    = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator       = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;
};

using
deque_types =
    Types<
    deque<int>,
    my_deque<int>,
    deque<int, allocator<int>>,
    my_deque<int, allocator<int>>
    >;

#ifdef __APPLE__
TYPED_TEST_CASE(DequeFixture, deque_types,);
#else
TYPED_TEST_CASE(DequeFixture, deque_types);
#endif

// -----
// Tests
// -----

//---------------------
// iterator == operator
//---------------------


TYPED_TEST(DequeFixture, iterator_equal_test0) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(5);
    iterator b = begin(x);
    iterator e = end(x);
    ASSERT_FALSE(b == e);
}

TYPED_TEST(DequeFixture, iterator_equal_test1) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(5);
    iterator b1 = begin(x);
    iterator b2 = begin(x);
    ASSERT_TRUE(b1 == b2);
}


//------------------
// copy constructor
//------------------

TYPED_TEST(DequeFixture, copy_constructor0) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    for (int i = 1; i < 22; ++i) {
        x.push_back(i);
    }
    deque_type y(x);
    for (int i = 1; i < 22; ++i) {
        ASSERT_EQ (i, y[i-1]);
    }
}


//------------------
// deque [] operator
//------------------

TYPED_TEST(DequeFixture, access_test0) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(5);
    for (int i = 0; i < 5; ++i) {
        x[i] = i;
    }
    for (int i = 0; i < 5; ++i) {
        ASSERT_EQ(x[i], i);
    }
}

//----
// at
//----

TYPED_TEST(DequeFixture, at_test0) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(25);
    for (int i = 0; i < 25; ++i) {
        x[i] = i;
    }
    for (int i = 0; i < 25; ++i) {
        ASSERT_EQ(x[i], i);
    }
}

//-----
// back
//-----

TYPED_TEST(DequeFixture, back_test0) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    for (int i = 0; i < 25; ++i) {
        x.push_back(i);
    }
    ASSERT_EQ(x.back(), 24);
}

//------
// erase
//------

TYPED_TEST(DequeFixture, erase_test0) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(21);
    for (int i = 1; i < 22; ++i) {
        x[i-1] = i;
    }
    iterator b = x.begin();
    for (int i = 0; i < 10; ++i) {
        ++b;
    }
    b = x.erase(b);
    ASSERT_EQ(*b, 12);
    b = x.begin();
    for (int i = 1; i < 22; ++i) {
        if (i != 11) {
            ASSERT_EQ(*b, i);
            ++b;
        }
    }
    ASSERT_TRUE(b == x.end());
}

TYPED_TEST(DequeFixture, erase_test1) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(25);
    for (int i = 1; i < 26; ++i) {
        x[i-1] = i;
    }
    iterator b = x.begin();
    b = x.erase(b);
    ASSERT_EQ(*b, 2);
    ASSERT_EQ(x.size(), 24);
    b = x.begin();
    for (int i = 2; i < 26; ++i) {
        ASSERT_EQ(*b, i);
        ++b;
    }
    ASSERT_TRUE(b == x.end());
}


//------
// front
//------

TYPED_TEST(DequeFixture, front_test0) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(25);
    for (int i = 0; i < 25; ++i) {
        x[i] = i+1;
    }
    ASSERT_EQ(x.front(), 1);
}

//-------
// insert
//-------

TYPED_TEST(DequeFixture, insert_test0) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(10);
    for (int i = 0; i < 10; ++i) {
        x[i] = i+1;
    }
    iterator b = x.begin();
    b = x.insert(b, 0);
    ASSERT_EQ(x.size(), 11);
    ASSERT_EQ(x.front(), 0);
    ASSERT_EQ(*b, 0);
    for (unsigned long i = 0; i < x.size(); ++i) {
        ASSERT_EQ(*b, i);
        ++b;
    }
    ASSERT_TRUE(b == x.end());
}

TYPED_TEST(DequeFixture, insert_test1) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(10);
    for (int i = 0; i < 10; ++i) {
        x[i] = i+1;
    }
    iterator e = x.end();
    e = x.insert(e, 11);
    ASSERT_EQ(x.size(), 11);
    ASSERT_EQ(x.back(), 11);
    ASSERT_EQ(*e, 11);
    iterator b = x.begin();
    for (unsigned long i = 1; i <= x.size(); ++i) {
        ASSERT_EQ(*b, i);
        ++b;
    }
    ASSERT_TRUE(b == x.end());
}


//----------
// pop_back
//----------

TYPED_TEST(DequeFixture, pop_back_test0) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(25);
    for (int i = 0; i < 25; ++i) {
        x[i] = i+1;
    }
    for (int i = 25; i > 0; --i) {
        ASSERT_EQ(x.back(), i);
        x.pop_back();
    }
    ASSERT_TRUE(x.empty());
}

//----------
// pop_front
//----------

TYPED_TEST(DequeFixture, pop_front_test0) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(25);
    for (int i = 0; i < 25; ++i) {
        x[i] = i+1;
    }
    for (int i = 1; i <= 25; ++i) {
        ASSERT_EQ(x.front(), i);
        x.pop_front();
    }
    ASSERT_TRUE(x.empty());
}

//----------
// push_back
//----------

TYPED_TEST(DequeFixture, push_back_test0) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    for (int i = 1; i <= 25; ++i) {
        x.push_back(i);
    }
    for (int i = 25; i > 0; --i) {
        ASSERT_EQ(x.back(), i);
        x.pop_back();
    }
    ASSERT_TRUE(x.empty());
}




//----------
// push_front
//----------

TYPED_TEST(DequeFixture, push_front_test0) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    for (int i = 25; i > 0; --i) {
        x.push_front(i);
    }
    for (int i = 1; i <= 25; ++i) {
        ASSERT_EQ(x.front(), i);
        x.pop_front();
    }
    ASSERT_TRUE(x.empty());
}

TYPED_TEST(DequeFixture, push_front_test1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    for (int i = 1; i <= 25; ++i) {
        x.push_front(i);
    }
    for (int i = 1; i <= 25; ++i) {
        x.push_back(i);
    }
    auto b = x.begin();
    for (int i = 25; i > 0; --i) {
        ASSERT_EQ(*b, i);
        ++b;
    }
    for (int i = 1; i <= 25; ++i) {
        ASSERT_EQ(*b, i);
        ++b;
    }
    ASSERT_TRUE(b == x.end());
}

TYPED_TEST(DequeFixture, push_front_test2) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1});
    for (int i = 2; i <= 1000; ++i) {
        x.push_front(i);
        x.pop_front();
    }
    for (int i = 2; i <= 1000; ++i) {
        x.push_back(i);
        x.pop_back();
    }
    auto b = x.begin();
    ASSERT_EQ(*b, 1);
    ++b;
    ASSERT_TRUE(b == x.end());
}

//--------------------
// iterator * operator
//--------------------

TYPED_TEST(DequeFixture, iterator_star_test0) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(21);
    for (int i = 0; i < 21; ++i) {
        x[i] = i+1;
    }
    int i = 21;
    for (iterator b = begin(x); b != end(x); ++b) {
        *b = i;
        --i;
    }
    iterator b2 = begin(x);
    for (int i = 21; i > 0; --i) {
        ASSERT_EQ(*b2, i);
        ++b2;
    }
}

//---------------------
// iterator ++ operator
//---------------------


TYPED_TEST(DequeFixture, iterator_increment_test0) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(21);
    for (int i = 0; i < 21; ++i) {
        x[i] = i+1;
    }
    iterator b = begin(x);
    for (int i = 0; i < 21; ++i) {
        ASSERT_EQ(*b, i+1);
        ++b;
    }
}

TYPED_TEST(DequeFixture, iterator_increment_test1) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(21);
    for (int i = 0; i < 21; ++i) {
        x[i] = i+1;
    }
    iterator b = begin(x);
    iterator e = end(x);
    int i = 0;
    while (b != e) {
        ASSERT_EQ(*b, ++i);
        ++b;
    }
}

//---------------------
// iterator -- operator
//---------------------


TYPED_TEST(DequeFixture, iterator_decrement_test0) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(21);
    for (int i = 0; i < 21; ++i) {
        x[i] = i+1;
    }
    iterator e = end(x);
    --e;
    for (int i = 21; i > 0; --i) {
        ASSERT_EQ(*e, i);
        --e;
    }
}

TYPED_TEST(DequeFixture, iterator_decrement_test1) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(21);
    for (int i = 0; i < 21; ++i) {
        x[i] = i+1;
    }
    iterator b = begin(x);
    iterator e = end(x);
    int i = 22;
    while (b != e) {
        --e;
        ASSERT_EQ(*e, --i);
    }
}

//---------------------
// iterator += operator
//---------------------

TYPED_TEST(DequeFixture, iterator_plus_equal_test0) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(21);
    for (int i = 0; i < 21; ++i) {
        x[i] = i+1;
    }
    iterator b = begin(x);
    b += 20;
    ASSERT_EQ(*b, 21);
}

//---------------------
// iterator -= operator
//---------------------


TYPED_TEST(DequeFixture, iterator_minus_equal_test0) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(21);
    for (int i = 0; i < 21; ++i) {
        x[i] = i+1;
    }
    iterator e = end(x);
    e -= 20;
    ASSERT_EQ(*e, 2);
}

TYPED_TEST(DequeFixture, iterator_minus_equal_test1) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x(15);
    for (int i = 0; i < 15; ++i) {
        x[i] = i+1;
    }
    iterator b = begin(x);
    iterator e = end(x);
    b += 9;
    e -= 6;
    ASSERT_TRUE(e == b);
}

//---------------------------
// const_iterator == operator
//---------------------------

TYPED_TEST(DequeFixture, const_iterator_equal_test0) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x;
    const_iterator b = begin(x);
    const_iterator e = end(x);
    ASSERT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, const_iterator_equal_test1) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x(5);
    const_iterator b = begin(x);
    const_iterator e = end(x);
    ASSERT_FALSE(b == e);
}

//--------------------------
// const_iterator * operator
//--------------------------

TYPED_TEST(DequeFixture, const_iterator_star_test0) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x({1, 2, 3, 4, 5});
    const_iterator b = begin(x);
    ASSERT_EQ(*b, 1);
}

//---------------------------
// const_iterator ++ operator
//---------------------------


TYPED_TEST(DequeFixture, const_iterator_increment_test0) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15});
    const_iterator b = begin(x);
    const_iterator e = end(x);
    int i = 0;
    while (b != e) {
        ASSERT_EQ(*b, ++i);
        ++b;
    }
}

//---------------------------
// const_iterator -- operator
//---------------------------

TYPED_TEST(DequeFixture, const_iterator_decrement_test0) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15});
    const_iterator b = begin(x);
    const_iterator e = end(x);
    int i = 16;
    while (b != e) {
        --e;
        ASSERT_EQ(*e, --i);
    }
}

//---------------------------
// const_iterator += operator
//---------------------------

TYPED_TEST(DequeFixture, const_iterator_plus_equal_test0) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15});
    const_iterator b = begin(x);
    b += 14;
    ASSERT_EQ(*b, 15);
}

//---------------------------
// const_iterator -= operator
//---------------------------

TYPED_TEST(DequeFixture, const_iterator_minus_equal_test0) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15});
    const_iterator b = begin(x);
    const_iterator e = end(x);
    b += 9;
    e -= 6;
    ASSERT_TRUE(e == b);
}

//---------------------
// my_deque == operator
//---------------------

TYPED_TEST(DequeFixture, my_deque_equal_test0) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12});
    deque_type y({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12});
    ASSERT_EQ(x, y);
}

TYPED_TEST(DequeFixture, my_deque_equal_test1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13});
    deque_type y({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12});
    ASSERT_NE(x, y);
}

TYPED_TEST(DequeFixture, my_deque_equal_test2) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12});
    deque_type y({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13});
    ASSERT_NE(x, y);
}

//---------------------
// my_deque < operator
//---------------------

TYPED_TEST(DequeFixture, my_deque_less_than_test0) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12});
    deque_type y({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12});
    ASSERT_FALSE(x < y);
}

TYPED_TEST(DequeFixture, my_deque_less_than_test1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13});
    deque_type y({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12});
    ASSERT_FALSE(x < y);
}

TYPED_TEST(DequeFixture, my_deque_less_than_test2) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12});
    deque_type y({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13});
    ASSERT_TRUE(x < y);
}

//--------
// resize
//--------

TYPED_TEST(DequeFixture, resize_test0) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12});
    x.resize(12);
    ASSERT_EQ(x.size(), 12);
    int i = 0;
    for (iterator it = x.begin(); it != x.end(); ++it) {
        ASSERT_EQ(*it, ++i);
    }
}

TYPED_TEST(DequeFixture, resize_test1) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12});
    x.resize(1);
    ASSERT_EQ(x.size(), 1);
    for (iterator it = x.begin(); it != x.end(); ++it) {
        ASSERT_EQ(*it, 1);
    }
}

TYPED_TEST(DequeFixture, resize_test2) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12});
    x.resize(20);
    ASSERT_EQ(x.size(), 20);
    for (int i = 1; i < 13; ++i) {
        ASSERT_EQ(x[i-1], i);
    }
    for (int i = 13; i < 21; ++i) {
        ASSERT_EQ(x[i-1], 0);
    }
}

TYPED_TEST(DequeFixture, resize_test3) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12});
    x.resize(25);
    ASSERT_EQ(x.size(), 25);
    for (int i = 1; i < 13; ++i) {
        ASSERT_EQ(x[i-1], i);
    }
    for (int i = 13; i < 26; ++i) {
        ASSERT_EQ(x[i-1], 0);
    }
}

//--------------------
// my_deque = operator
//--------------------

TYPED_TEST(DequeFixture, equals_test0) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12});
    x = x;
    iterator b = x.begin();
    for (int i = 1; i < 13; ++i) {
        ASSERT_EQ (i, *b);
        ++b;
    }
    ASSERT_TRUE (b == x.end());
}

TYPED_TEST(DequeFixture, equals_test1) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12});
    deque_type y({0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11});
    y = x;
    iterator b = y.begin();
    for (int i = 1; i < 13; ++i) {
        ASSERT_EQ (i, *b);
        ++b;
    }
    ASSERT_TRUE (b == y.end());
}

TYPED_TEST(DequeFixture, equals_test2) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11});
    deque_type y({0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 999});
    y = x;
    iterator b = y.begin();
    for (int i = 1; i < 12; ++i) {
        ASSERT_EQ (i, *b);
        ++b;
    }
    ASSERT_TRUE (b == y.end());
}

TYPED_TEST(DequeFixture, equals_test3) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19});
    deque_type y({0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 999});
    y = x;
    iterator b = y.begin();
    for (int i = 1; i < 20; ++i) {
        ASSERT_EQ (i, *b);
        ++b;
    }
    ASSERT_TRUE (b == y.end());
}

TYPED_TEST(DequeFixture, equals_test4) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12});
    deque_type y({0, 1, 2, 3, 4});
    y = x;
    iterator b = y.begin();
    for (int i = 1; i < 13; ++i) {
        ASSERT_EQ (i, *b);
        ++b;
    }
    ASSERT_TRUE (b == y.end());
}

//------
// clear
//------

TYPED_TEST(DequeFixture, clear_test0) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12});
    x.clear();
    ASSERT_EQ(x.size(), 0);
    for (iterator it = x.begin(); it != x.end(); ++it) {
        ASSERT_TRUE (false);
    }
    ASSERT_TRUE (true);
}

//------
// swap
//------

TYPED_TEST(DequeFixture, swap_test0) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12});
    deque_type y({7, 8, 9});
    x.swap(y);
    iterator b = y.begin();
    for (int i = 1; i < 13; ++i) {
        ASSERT_EQ (i, *b);
        ++b;
    }
    ASSERT_TRUE (b == y.end());
    iterator b2 = x.begin();
    for (int i = 7; i < 10; ++i) {
        ASSERT_EQ (i, *b2);
        ++b2;
    }
    ASSERT_TRUE (b2 == x.end());
}

TYPED_TEST(DequeFixture, swap_test1) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12});
    deque_type y({7, 8, 9}, allocator<char>());
    x.swap(y);
    iterator b = y.begin();
    for (int i = 1; i < 13; ++i) {
        ASSERT_EQ (i, *b);
        ++b;
    }
    ASSERT_TRUE (b == y.end());
    iterator b2 = x.begin();
    for (int i = 7; i < 10; ++i) {
        ASSERT_EQ (i, *b2);
        ++b2;
    }
    ASSERT_TRUE (b2 == x.end());
}
