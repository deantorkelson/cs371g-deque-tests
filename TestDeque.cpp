// -------------
// TestDeque.c++
// -------------

// http://www.cplusplus.com/reference/deque/deque/

// --------
// includes
// --------

#include <deque>     // deque
#include <stdexcept> // invalid_argument, out_of_range

#include "gtest/gtest.h"

#include "Deque.hpp"

// -----
// Using
// -----

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct DequeFixture : Test {
    using deque_type    = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator       = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;
};

using
deque_types =
    Types<
    // deque<int>,
    my_deque<int>>;
//  deque<int, allocator<int>>>;
//my_deque<int, allocator<int>>>;

#ifdef __APPLE__
TYPED_TEST_CASE(DequeFixture, deque_types,);
#else
TYPED_TEST_CASE(DequeFixture, deque_types);
#endif

// -----
// Tests
// -----

TYPED_TEST(DequeFixture, empty) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x;
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0u);
}

TYPED_TEST(DequeFixture, push_back) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(0);
    x.push_back(2);
    x.push_back(4);
    x.push_back(5);
    ASSERT_EQ(x[0], 0);
    ASSERT_EQ(x[1], 2);
    ASSERT_EQ(x[2], 4);
    ASSERT_EQ(x[3], 5);
    ASSERT_TRUE(!x.empty());
    ASSERT_EQ(x.size(), 4);
}

TYPED_TEST(DequeFixture, push_front) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_front(1);
    x.push_front(2);
    x.push_front(4);
    x.push_front(5);
    //cout << x[0] << " " << x[1] << " " << x[2] << " " << x[3] << endl;
    ASSERT_EQ(x[3], 1);
    ASSERT_EQ(x[2], 2);
    ASSERT_EQ(x[1], 4);
    ASSERT_EQ(x[0], 5);

    ASSERT_TRUE(!x.empty());
    ASSERT_EQ(x.size(), 4);
}

TYPED_TEST(DequeFixture, iterator_plus) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    srand(time(0));
    for(int i = 0; i < 100; ++i)
        x.push_front(rand() % 100);
    typename deque_type::iterator it = x.begin();
    for(int i = 0; i < 100; ++i)
    {
        ASSERT_EQ(*(it + i), x[i]);
    }

    ASSERT_TRUE(!x.empty());
    ASSERT_EQ(x.size(), 100);
}


TYPED_TEST(DequeFixture, pop_front) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    std::deque<int> y;
    int op = 0;
    while(op < 100)
    {
        int val = rand();
        x.push_front(val);
        y.push_front(val);
        ++op;
        if(rand() % 4)
        {
            int pop = rand() % x.size();
            while(pop)
            {

                ASSERT_EQ(x.front(), y.front());
                x.pop_front();
                y.pop_front();
                --pop;
                ++op;
            }

        }
    }

    ASSERT_TRUE(x.size() == y.size());
}

TYPED_TEST(DequeFixture, pop_back) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    std::deque<int> y;
    int op = 0;
    while(op < 100)
    {
        int val = rand();
        x.push_back(val);
        y.push_back(val);
        ++op;
        if(rand() % 4)
        {
            int pop = rand() % x.size();
            while(pop)
            {

                ASSERT_EQ(x.back(), y.back());
                x.pop_back();
                y.pop_back();
                --pop;
                ++op;
            }

        }
    }

    ASSERT_TRUE(x.size() == y.size());
}
TYPED_TEST(DequeFixture, at) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(0);
    x.push_back(2);
    x.push_front(9);
    x.push_back(6);
    x.push_front(1);
    ASSERT_EQ(x[0], 1);
    ASSERT_EQ(x[1], 9);
    ASSERT_EQ(x[2], 0);
    ASSERT_EQ(x[3], 2);
    ASSERT_EQ(x[4], 6);
    ASSERT_TRUE(!x.empty());
    ASSERT_EQ(x.size(), 5);
    try
    {
        x.at(5);
    }
    catch(out_of_range& e) {
        return;
    }
    ASSERT_TRUE(false);
}

TYPED_TEST(DequeFixture, iterator_minus) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;

    for(int i = 0; i < 100; ++i)
        x.push_back(rand() % 100);
    typename deque_type::iterator it = x.end();
    for(int i = 1; i < 100; ++i)
    {
        ASSERT_EQ(*(it - i), x[100 - i]);
    }

    ASSERT_TRUE(!x.empty());
    ASSERT_EQ(x.size(), 100);
}

TYPED_TEST(DequeFixture, equal) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    deque_type y;
    std::deque<int> z;
    for(int i = 0; i < 50; ++i)
    {
        int val = rand() % 100;
        if(rand() % 2)
        {
            x.push_back(val);
            y.push_back(val);
            z.push_back(val);
        }
        else
        {
            x.push_front(val);
            y.push_front(val);
            z.push_front(val);
        }
    }

    ASSERT_EQ(x, y);
    ASSERT_TRUE(equal(x.begin(), x.end(), z.begin(), z.end()));
}

TYPED_TEST(DequeFixture, constructor1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(100);
    deque_type y;
    for(int i = 0; i < 100; ++i)
    {
        y.push_back(int());
    }

    ASSERT_EQ(x, y);
}

TYPED_TEST(DequeFixture, constructor2) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(100, 5);
    deque_type y;
    for(int i = 0; i < 100; ++i)
    {
        y.push_back(5);
    }

    ASSERT_EQ(x, y);
}

TYPED_TEST(DequeFixture, constructor3) {
    using deque_type = typename TestFixture::deque_type;
    allocator<int> a;
    deque_type x(100, 5, a);
    deque_type y;
    for(int i = 0; i < 100; ++i)
    {
        y.push_back(5);
    }

    ASSERT_EQ(x, y);
}

TYPED_TEST(DequeFixture, constructor4) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3, 4, 5, 6, 7});
    deque_type y;
    for(int i = 1; i <= 7; ++i)
    {
        y.push_back(i);
    }

    ASSERT_EQ(x, y);
}

TYPED_TEST(DequeFixture, constructor5) {
    using deque_type = typename TestFixture::deque_type;
    allocator<int> a;
    deque_type x({1, 2, 3, 4, 5, 6, 7}, a);
    deque_type y;
    for(int i = 1; i <= 7; ++i)
    {
        y.push_back(i);
    }

    ASSERT_EQ(x, y);
}

TYPED_TEST(DequeFixture, constructor6) {
    using deque_type = typename TestFixture::deque_type;
    deque_type y;
    for(int i = 0; i < 100; ++i)
    {
        y.push_front(rand() % 1000);
    }
    deque_type x(y);
    ASSERT_EQ(x, y);
}
TYPED_TEST(DequeFixture, insert1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(0);
    x.push_back(2);
    deque_type y(x);
    y.push_back(9);
    y.push_back(4);
    y.push_back(5);
    x.push_back(4);
    x.push_back(5);
    x.insert(x.begin() + 2, 9);
    ASSERT_EQ(x, y);
}

TYPED_TEST(DequeFixture, insert2) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    std::deque<int> y;
    for(int i = 0; i < 100; ++i)
    {
        int val = rand() % 100;
        int op = rand() % 3;
        switch(op)
        {
        case 0:
        {
            x.push_back(val);
            y.push_back(val);
            break;
        }
        case 1:
        {
            x.push_front(val);
            y.push_front(val);
            break;
        }
        case 2:
        {
            if(!x.size())
            {
                x.insert(x.begin(), val);
                y.insert(y.begin(), val);
            }
            else
            {
                int index = rand() % (x.size());
                x.insert(x.begin() + index, val);
                y.insert(y.begin() + index, val);
            }
            break;
        }
        }
    }

    for(int i = 0; i < x.size(); ++i)
        ASSERT_EQ(x[i], y[i]);


}

TYPED_TEST(DequeFixture, erase1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(0);
    x.push_back(2);
    deque_type y(x);
    x.push_back(3);
    x.push_back(4);
    y.push_back(5);
    y.push_back(6);
    x.push_back(5);
    x.push_back(6);
    x.erase(x.begin() + 2);
    x.erase(x.begin() + 2);
    ASSERT_EQ(x, y);
}

TYPED_TEST(DequeFixture, erase2) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    std::deque<int> y;
    for(int i = 0; i < 100; ++i)
    {
        int val = rand() % 100;
        int op = rand() % 3;
        switch(op)
        {
        case 0:
        {
            x.push_back(val);
            y.push_back(val);
            break;
        }
        case 1:
        {
            x.push_front(val);
            y.push_front(val);
            break;
        }
        case 2:
        {
            if(x.size())
            {
                int index = rand() % (x.size());
                x.erase(x.begin() + index);
                y.erase(y.begin() + index);
            }
            break;
        }
        }
    }

    for(int i = 0; i < x.size(); ++i)
        ASSERT_EQ(x[i], y[i]);
}

TYPED_TEST(DequeFixture, clear1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    deque_type y({1, 4, 6, 10});
    x.push_back(9);
    x.push_back(0);
    x.push_front(1);
    x.push_back(3);
    x.clear();

    x.push_back(6);
    x.push_back(10);
    x.push_front(4);
    x.push_front(1);
    ASSERT_EQ(x, y);
}

TYPED_TEST(DequeFixture, clear2) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    std::deque<int> y;
    for(int i = 0; i < 100; ++i)
    {
        int val = rand() % 100;
        int op = rand() % 3;
        switch(op)
        {
        case 0:
        {
            x.push_back(val);
            y.push_back(val);
            break;
        }
        case 1:
        {
            x.push_front(val);
            y.push_front(val);
            break;
        }
        case 2:
        {
            x.clear();
            y.clear();
            break;
        }
        }
    }

    ASSERT_TRUE(std::equal(x.begin(), x.end(), y.begin(), y.end()));
}

TYPED_TEST(DequeFixture, swap1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({2, 4, 6, 8, 10});
    deque_type y({1, 3, 5, 7, 9});
    deque_type z = x;
    deque_type w = y;
    swap(x, y);
    ASSERT_EQ(x, w);
    ASSERT_EQ(y, z);
}

TYPED_TEST(DequeFixture, swap2) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({2, 4, 6, 8, 10, 12, 14});
    deque_type y({1, 3, 5, 7, 9});
    deque_type z = x;
    deque_type w = y;
    x.swap(y);
    ASSERT_EQ(x, w);
    ASSERT_EQ(y, z);
}

TYPED_TEST(DequeFixture, swap3) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({2, 4, 6, 8, 10});
    deque_type y({1, 3, 5, 7, 9, 11, 13, 15, 17, 19});
    deque_type z(x);
    deque_type w(y);
    x.swap(y);
    ASSERT_EQ(x, w);
    ASSERT_EQ(y, z);
}

TYPED_TEST(DequeFixture, swap4) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    deque_type y;
    swap(x, y);
    ASSERT_EQ(x, y);
}

TYPED_TEST(DequeFixture, resize1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.resize(10);
    deque_type y({0, 0, 0, 0, 0, 0, 0, 0, 0, 0});
    ASSERT_EQ(x, y);
}

TYPED_TEST(DequeFixture, resize2) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.resize(10, 5);
    deque_type y({5, 5, 5, 5, 5, 5, 5, 5, 5, 5});
    ASSERT_EQ(x, y);
}

TYPED_TEST(DequeFixture, resize3) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.resize(5, 5);
    x.push_front(3);
    x.push_back(6);
    x.push_front(2);
    x.push_back(7);
    deque_type y({2, 3, 5, 5, 5, 5, 5, 6, 7});
    ASSERT_EQ(x, y);
}

TYPED_TEST(DequeFixture, resize4) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1, 2, 3, 4, 5, 6});
    x.push_front(-1);
    x.push_back(8);
    x.resize(4);
    deque_type y({-1, 1, 2, 3});
    ASSERT_EQ(x, y);
}


TYPED_TEST(DequeFixture, test2) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    iterator b = begin(x);
    iterator e = end(x);
    ASSERT_EQ(b, e);
}

TYPED_TEST(DequeFixture, test3) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x;
    const_iterator b = begin(x);
    const_iterator e = end(x);
    ASSERT_EQ(b, e);
}
